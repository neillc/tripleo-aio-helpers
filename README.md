A quick and dirty script to help set up a RHOSP AIO

This has grown a bit. At some point I will make this a proper readme, but for now a few notes:

The goal is to build a VM (although you could use baremetal) and deploy a RHOSP AIO to it. Then to add a project, flavors, images, networks to support testing os-migrate.

After that create a VM to migrate, a conversion host to do the migration.

This may well be useful for other things, but this is the current aim.

I am primarily targetting RHEL8.4 and RHOSP16.2. As a result I'm tied to virt-install v3.2.0 which makes using cloud-init for configuring network on the AIO VM a little annoying. Later versions allow you to pass in a network config, but 3.2.0 means I have to use nmcli and bootcmd to get the network configure.

There is a vestigial network-config.tpl in the virt-install directory but it is not actually used yet.

EVerything is a bit of a mess but here's a brief overview:

main.py - original script. Generates the yaml files and a dploy script to deploy a RHOSP AIO. Assumes a VM to run on. Don't run this on your laptop. You'll be sorry if you do.

os_migrate_setup.py - build the OSP infrastructure to allow testing os-migrate.

os_migrate_teardown.py - remove that infrastructure (to allow for refinement/iteration)

virt-install/create_aio_vm.py - use virt-install to create and configure a VM to run the other scripts on.

```
usage: main.py [-h] -u USERNAME -p PASSWORD -a ADDRESS [-i INTERFACE]
               [-m NETMASK] -d DNS [DNS ...] [-D DOMAIN]
               [--using-multiple-nics] [-U DEPLOYMENT_USER]
               [--deployment-dir DEPLOYMENT_DIR] [-g GATEWAY]
               [--containers-yaml-out CONTAINERS_YAML_OUT]
               [--standalone-yaml-out STANDALONE_YAML_OUT]

optional arguments:
  -h, --help            show this help message and exit
  -u USERNAME, --username USERNAME
  -p PASSWORD, --password PASSWORD
  -a ADDRESS, --address ADDRESS
  -i INTERFACE, --interface INTERFACE
  -m NETMASK, --netmask NETMASK
  -d DNS [DNS ...], --dns DNS [DNS ...]
  -D DOMAIN, --domain DOMAIN
  --using-multiple-nics
  -U DEPLOYMENT_USER, --deployment-user DEPLOYMENT_USER
  --deployment-dir DEPLOYMENT_DIR
  -g GATEWAY, --gateway GATEWAY
  --containers-yaml-out CONTAINERS_YAML_OUT
  --standalone-yaml-out STANDALONE_YAML_OUT
```

Use this script to generate the yaml and a deploy script to build an AIO

By default the files will be placed in the home directory of the user running the script (should probable be the stack user)

When happy with the contents rename them to remove the .gen suffix and run the deploy script.

The doco for the AIO deply says to run the deploy using sudo - I don't think this is correct. If you use sudo you will get a warning about files ending up /root.