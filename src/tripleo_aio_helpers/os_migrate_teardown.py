"""
Quick and dirty script to help setup project, flavors, networks, images
"""
import argparse
import json

# import os
import subprocess


def openstack_cmd(cmd, args):
    """Utility function to run an openstack command agains the standalone cloud"""
    cmd = "OS_CLOUD=standalone openstack " + cmd

    if args.dry_run:
        print("dry-run specified. Not executing cmd. cmd was:")
        print(cmd)
        return

    result = subprocess.check_output(cmd, shell=True, universal_newlines=True)
    return result


def parse_args():
    """Parse the command line arguments"""

    # home = os.environ.get('HOME')

    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--project-domain", default="default")
    parser.add_argument("-n", "--project-name", default="test-project")
    parser.add_argument("-D", "--project-description", default="Test project")
    parser.add_argument("-u", "--username", default="test-user")
    parser.add_argument("-p", "--password", default="secrete123")
    parser.add_argument("-c", "--cloud", default="standalone")
    parser.add_argument("-g", "--gateway", default="10.76.23.254")
    parser.add_argument(
        "-C", "--public-network-cider", default="10.76.23.0/24"
    )
    parser.add_argument("--private-network-cidr", default="192.168.100.0/24")
    parser.add_argument("--publice-net-start", default="10.76.23.50")
    parser.add_argument("--publice-net-end", default="10.76.23.52")
    parser.add_argument("--dns-server", default="10.76.23.245")

    # export OS_CLOUD=standalone
    # export STANDALONE_HOST=10.76.23.39

    args = parser.parse_args()

    return args


def destroy_project(args):
    """Delete the project if it exists"""
    cmd = "project list -f json"
    project_exists = [
        x
        for x in json.loads(openstack_cmd(cmd, args))
        if x["Name"] == args.project_name
    ]

    if project_exists:
        print(f"Project {args.project_name} exists. Will delete")
        args.project_id = project_exists[0]["ID"]

        cmd = (
            f"project delete -f json --domain {args.project_domain} "
            f"{args.project_id}"
        )
        print(f"Project {args.project_name} deleted")
    else:
        print("Project {args.project_name} not found.")


def destroy_user(args):
    """Delete the user if it exists"""
    cmd = "user list -f json"
    user_exists = [
        x for x in json.loads(openstack_cmd(cmd, args)) if x["Name"] == args.username
    ]

    if user_exists:
        print(f"User {args.username} already exists. Skipping creation")
        args.user_id = user_exists[0]["ID"]
        return

    cmd = (
        f"user create  -f json --project {args.project_id} "
        f"--password {args.password} {args.username}"
    )

    args.user_id = json.loads(openstack_cmd(cmd, args))["id"]

    print(f"User created - id: {args.user_id}")


def assign_member_role(args):
    """
    Assign the member role to the user.

    Note: it doesn't matter if the role is assigned multiple times, so not
    bothering to check.
    """

    cmd = f"role add --user {args.username} --project {args.project_id} member"

    result = openstack_cmd(cmd, args)

    cmd = f"role assignment list --user {args.user_id} --role member -f json"
    result = json.loads(openstack_cmd(cmd, args))

    if result:
        print("User has member role")


def destroy_public_network(args):
    """Coming soon - create the public network"""
    # pylint: disable=unused-argument,unused-variable
    print("creating public network - NYI")
    cmd = (
        "network create --external --provider-physical-network datacentre "
        "--provider-network-type flat public"
    )
    cmd = (
        f"subnet create public-net --subnet-range {args.publice_network_cidr} "
        f"--no-dhcp --gateway {args.gateway} --allocation-pool "
        f"start={args.public_net_start},end={args.public_net_end} "
        "--network public"
    )


def destroy_private_network(args):
    """Coming soon - create the private network"""
    # pylint: disable=unused-argument,unused-variable
    cmd = "openstack network create --internal private"
    cmd = (
        "openstack subnet create private-net "
        f"--subnet-range {args.private_network_cidr} --network private"
    )
    print("creating private network - NYI")


def destroy_cirros_flavor(args):
    """Coming soon - create the cirros flavor"""
    # pylint: disable=unused-argument
    print("creating cirros flavor - NYI")


def destroy_rhel_flavor(args):
    """Coming soon - create the rhel flavor"""
    # pylint: disable=unused-argument
    print("creating rhel flavor - NYI")


def destroy_cirros_image(args):
    """Coming soon - create the cirros image"""
    # pylint: disable=unused-argument
    print("creating cirros image - NYI")


def destroy_rhel_image(args):
    """Coming soon - create the rhel image"""
    # pylint: disable=unused-argument
    print("creating rhel image - NYI")


def destroy_cirros_instance(args):
    """Coming soon - create the cirros instance"""
    # pylint: disable=unused-argument
    print("creating cirros instance - NYI")


def destroy_rhel_instance(args):
    """Coming soon - create the rhel instance"""
    # pylint: disable=unused-argument
    print("creating rhel instance - NYI")


def main():
    """main function"""
    args = parse_args()

    # destroy_cirros_instance(args)
    # destroy_rhel_instance(args)

    # destroy_cirros_image(args)
    # destroy_rhel_image(args)

    # destroy_cirros_flavor(args)
    # destroy_rhel_flavor(args)

    # destroy_public_network(args)
    # destroy_private_network(args)

    destroy_user(args)
    destroy_project(args)


if __name__ == "__main__":
    main()
