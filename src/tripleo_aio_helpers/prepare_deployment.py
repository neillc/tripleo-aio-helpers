"""
Quick and dirty script to help setup config filers for a RHOSP AIO install
"""
import argparse
import os
import subprocess
import yaml


def parse_args():
    """Parse the command line arguments"""

    home = os.environ.get("HOME")

    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--username", required=True)
    parser.add_argument("-p", "--password", required=True)
    parser.add_argument("-a", "--address", required=True)
    parser.add_argument("-i", "--interface", required=True)
    parser.add_argument("-m", "--netmask", default=24)
    parser.add_argument(
        "-d", "--dns", nargs="+", action="append", required=True
    )
    parser.add_argument("-D", "--domain", default="aio")
    parser.add_argument("--using-multiple-nics", action="store_true")
    parser.add_argument("-U", "--deployment-user")
    parser.add_argument("--deployment-dir")
    parser.add_argument("-g", "--gateway")
    parser.add_argument(
        "--containers-yaml-out",
        default=f"{home}/containers-prepare-parameters.yaml.gen",
    )
    parser.add_argument(
        "--standalone-yaml-out",
        default=f"{home}/standalone_parameters.yaml.gen",
    )
    parser.add_argument(
        "--deploy-script-out", default=f"{home}/deploy.sh.gen", dest="deploy"
    )

    args = parser.parse_args()

    if not args.deployment_user:
        args.deployment_user = os.environ.get("USER")

    if not args.deployment_dir:
        args.deployment_dir = os.environ.get("HOME")

    if not args.using_multiple_nics and not args.gateway:
        raise ValueError("Must specify a gateway when using a single nic")

    return args


def get_standalone_parameters(args):
    """Build the standalone parameters as a dict ready to serialize as yaml"""
    sp_dict = {
        "parameter_defaults": {
            "CloudName": args.address,
            "CloudDomain": args.domain,
            "ControlPlaneStaticRoutes": [],
            "Debug": True,
            "DeploymentUser": args.deployment_user,
            "DnsServers": [dns[0] for dns in args.dns],
            "NeutronPublicInterface": args.interface,
            "NeutronDnsDomain": "localdomain",
            "NeutronBridgeMappings": "datacentre:br-ctlplane",
            "NeutronPhysicalBridge": "br-ctlplane",
            "StandaloneEnableRoutedNetworks": False,
            "StandaloneHomeDir": args.deployment_dir,
            "StandaloneLocalMtu": 1500,
        }
    }

    if not args.using_multiple_nics:
        sp_dict["parameter_defaults"]["ControlPlaneStaticRoutes"] = [
            {
                "ip_netmask": "0.0.0.0/0",
                "next_hop": args.gateway,
                "default": True,
            }
        ]

    return sp_dict


def build_containers_yaml(args):
    """Build the containers yaml based on a previously generated file."""

    cmd = "sudo openstack tripleo container image prepare default"
    result = subprocess.check_output(cmd, shell=True, universal_newlines=True)

    data = yaml.load(result, Loader=yaml.Loader)

    data["parameter_defaults"]["ContainerImageRegistryCredentials"] = {
        "registry.redhat.io": {args.username: args.password}
    }

    data["parameter_defaults"]["ContainerImageRegistryLogin"] = True

    return data


def deploy_sh(args):
    """Write out a deploy script"""
    home = os.environ.get("HOME")

    template = (
        "sudo openstack tripleo deploy \\\n"
        "  --templates \\\n"
        f"  --local-ip={args.address}/{args.netmask} \\\n"
        "  -e /usr/share/openstack-tripleo-heat-templates/"
        "environments/standalone/standalone-tripleo.yaml \\\n"
        "  -r /usr/share/openstack-tripleo-heat-templates/"
        "roles/Standalone.yaml \\\n"
        f"  -e {home}/containers-prepare-parameters.yaml \\\n"
        f"  -e {home}/standalone_parameters.yaml \\\n"
        f"  --output-dir {home} \\\n"
        "  --standalone\n"
    )

    return template


def set_hostname(args):
    """Set the hostname - NYI"""
    # pylint: disable=unused-argument
    print("set_hostname is not yet implemented")



def main():
    """main function"""
    args = parse_args()

    containers_yaml = build_containers_yaml(args)
    with open(
        args.containers_yaml_out, "w", encoding="utf-8"
    ) as containers_out:
        containers_out.write(yaml.dump(containers_yaml))
    print(f"containers yaml written to {args.containers_yaml_out}")

    standalone_parameters = get_standalone_parameters(args)
    with open(
        args.standalone_yaml_out, "w", encoding="utf-8"
    ) as parameters_out:
        parameters_out.write(yaml.dump(standalone_parameters))
    print(f"standalone parameters yaml written to {args.standalone_yaml_out}")

    with open(args.deploy, "w", encoding="utf-8") as deploy:
        deploy.write(deploy_sh(args))
    print(f"deploy script written to {args.deploy}")

    print("If you are running on a cloud image remenber to disable cloud-init before running the deploy.")
    print("sudo systemctl stop cloud-init")
    print("sudo systemctl disable cloud-init")
    print("Make sure you have specifed the correct interface to use!")


if __name__ == "__main__":
    main()
