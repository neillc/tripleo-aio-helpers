"""
Quick and dirty script to help setup project, flavors, networks, images
"""
import argparse
import json

import os
import subprocess
import sys


def openstack_cmd(cmd, args):
    """Utility function to run an openstack command agains the standalone cloud"""
    cmd = "OS_CLOUD=standalone openstack " + cmd

    if args.dry_run:
        print("dry-run specified. Not executing cmd. cmd was:")
        print(cmd)
        return
    
    result = subprocess.check_output(cmd, shell=True, universal_newlines=True)
    return result


def parse_args():
    """Parse the command line arguments"""

    # home = os.environ.get('HOME')

    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--project-domain", default="default")
    parser.add_argument("-n", "--project-name", default="test-project")
    parser.add_argument("-D", "--project-description", default="Test project")
    parser.add_argument("-u", "--username", default="test-user")
    parser.add_argument("-p", "--password", default="secrete123")
    parser.add_argument("-c", "--cloud", default="standalone")
    parser.add_argument("-g", "--gateway")
    parser.add_argument("-C", "--public-network-cidr")
    parser.add_argument("--private-network-cidr", default="192.168.100.0/24")
    parser.add_argument("--public-net-start")
    parser.add_argument("--public-net-end")
    parser.add_argument("--dns-server")
    parser.add_argument("--dry-run", action="store_true")

    # export OS_CLOUD=standalone
    # export STANDALONE_HOST=10.76.23.39

    args = parser.parse_args()

    if not args.gateway:
        args.gateway = get_from_env("--gateway", "AIO_GATEWAY")
    
    if not args.public_network_cidr:
        args.public_network_cidr = get_from_env("--public-network-cidr", "AIO_PUBLIC_CIDR")
    
    if not args.public_net_start:
        args.public_net_start = get_from_env("--public-net-start", "AIO_PUBLIC_NET_START")

    if not args.public_net_end:
        args.public_net_end = get_from_env("--public-net-end", "AIO_PUBLIC_NET_END")

    if not args.dns_server:
        args.dns_server = get_from_env("--dns-server", "AIO_DNS_SERVER")

    return args

def get_from_env(name, envvar):
    value = os.environ.get(envvar)

    if value is None:
        print(
            f"You must specify {name}, either on the commandline or using "
            f"the {envvar} environment varauble."
            )
        sys.exit(1)
    
    return value

def create_project(args):
    """Create the project if it doesn't already exist"""

    if args.dry_run:
        print("Dry run specified. Not creating project")
        return
    
    cmd = "project list -f json"
    project_exists = [
        x
        for x in json.loads(openstack_cmd(cmd, args))
        if x["Name"] == args.project_name
    ]

    if project_exists:
        print(f"Project {args.project_name} already exists. Skipping creation")
        args.project_id = project_exists[0]["ID"]
        return

    cmd = (
        f"project create -f json --description '{args.project_description}' "
        f"{args.project_name} --domain {args.project_domain}"
    )

    args.project_id = json.loads(openstack_cmd(cmd, args))["id"]

    print(f"Project created - id: {args.project_id}")


def create_user(args):
    """Create the user if it doesn't already exist"""

    if args.dry_run:
        print("Dry run specified. Not creating user")
        return
    
    cmd = "user list -f json"
    user_exists = [
        x for x in json.loads(openstack_cmd(cmd, args)) if x["Name"] == args.username
    ]

    if user_exists:
        print(f"User {args.username} already exists. Skipping creation")
        args.user_id = user_exists[0]["ID"]
        return

    cmd = (
        f"user create  -f json --project {args.project_id} "
        f"--password {args.password} {args.username}"
    )

    args.user_id = json.loads(openstack_cmd(cmd, args))["id"]

    print(f"User created - id: {args.user_id}")


def assign_member_role(args):
    """
    Assign the member role to the user.

    Note: it doesn't matter if the role is assigned multiple times, so not
    bothering to check.
    """

    if args.dry_run:
        print("Dry run specified. Not assigning role")
        return
    
    cmd = f"role add --user {args.username} --project {args.project_id} member"

    result = openstack_cmd(cmd, args)

    cmd = f"role assignment list --user {args.user_id} --role member -f json"
    result = json.loads(openstack_cmd(cmd, args))

    if result:
        print("User has member role")


def create_public_network(args):
    """Coming soon - create the public network"""
    # pylint: disable=unused-argument,unused-variable
    print("creating public network - NYI")
    cmd = (
        "network create --external --provider-physical-network datacentre "
        "--provider-network-type flat public"
    )
    cmd = (
        f"subnet create public-net --subnet-range {args.public_network_cidr} "
        f"--no-dhcp --gateway {args.gateway} --allocation-pool "
        f"start={args.public_net_start},end={args.public_net_end} "
        "--network public"
    )


def create_private_network(args):
    """Coming soon - create the private network"""
    # pylint: disable=unused-argument,unused-variable
    cmd = "openstack network create --internal private"
    cmd = (
        "openstack subnet create private-net "
        f"--subnet-range {args.private_network_cidr} --network private"
    )
    print("creating private network - NYI")


def create_cirros_flavor(args):
    """Coming soon - create the cirros flavor"""
    # pylint: disable=unused-argument
    print("creating cirros flavor - NYI")


def create_rhel_flavor(args):
    """Coming soon - create the rhel flavor"""
    # pylint: disable=unused-argument
    print("creating rhel flavor - NYI")


def create_cirros_image(args):
    """Coming soon - create the cirros image"""
    # pylint: disable=unused-argument
    print("creating cirros image - NYI")


def create_rhel_image(args):
    """Coming soon - create the rhel image"""
    # pylint: disable=unused-argument
    print("creating rhel image - NYI")


def create_cirros_instance(args):
    """Coming soon - create the cirros instance"""
    # pylint: disable=unused-argument
    print("creating cirros instance - NYI")


def create_rhel_instance(args):
    """Coming soon - create the rhel instance"""
    # pylint: disable=unused-argument
    print("creating rhel instance - NYI")


def main():
    """main function"""
    args = parse_args()

    create_project(args)

    create_user(args)
    assign_member_role(args)

    create_public_network(args)
    create_private_network(args)

    create_cirros_flavor(args)
    create_rhel_flavor(args)

    create_cirros_image(args)
    create_rhel_image(args)

    create_cirros_instance(args)
    create_rhel_instance(args)


if __name__ == "__main__":
    main()
