#cloud-config
system_info:
  default_user:
    name: stack
password: {data.password}
chpasswd: {{expire: False}}
ssh_pwauth: True
ssh_authorized_keys:
  - {data.public_key}
{data.bootcmd}
runcmd:
  - "subscription-manager register --username {data.rhn_user} --password '{data.rhn_password}'"
  - "subscription-manager release --set=8.4"
  - "dnf install -y dnf-utils"
  - "subscription-manager repos --disable=*"
  - "subscription-manager repos --enable=rhel-8-for-x86_64-baseos-eus-rpms --enable=rhel-8-for-x86_64-appstream-eus-rpms  --enable=rhel-8-for-x86_64-highavailability-eus-rpms  --enable=ansible-2.9-for-rhel-8-x86_64-rpms  --enable=openstack-16.2-for-rhel-8-x86_64-rpms  --enable=fast-datapath-for-rhel-8-x86_64-rpms"
  - "dnf module disable -y container-tools:rhel8"
  - "dnf module enable -y container-tools:3.0"
  - "dnf update -y"
  - "echo '***************************************************************************'"
  - "echo '***                                                                     ***'"
  - "echo '***                                                                     ***'"
  - "echo '***              DNF UPDATE COMPLETE - REBOOT NOW                       ***'"
  - "echo '***                                                                     ***'"
  - "echo '***                                                                     ***'"
  - "echo '***************************************************************************'"
  - "reboot"

