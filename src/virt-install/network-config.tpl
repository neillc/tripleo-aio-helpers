network:
  version: 1
  config:
    - type: physical
      name: en0
      subnets:
         - type: static
           address: {data.cidr_1}
           gateway: {data.gateway}
    - type: physical
      name: en1
      subnets:
         - type: static
           address: {data.cidr_2}
           gateway: {data.gateway}
    - type: nameserver
      address:
        - {data.dns}
      search:
        - {data.search_domain}